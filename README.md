# 使用DApp搭建服务器

```java
package com.da;

import com.da.core.app.DApp;
import com.da.core.app.impl.BIOApp;
import com.da.core.app.impl.NIOApp;
import com.da.core.context.MultipartFile;
import com.da.core.util.Utils;

/**
 * @author da
 * @time 2023/9/14 上午 11:45
 */
public class App {
    public static void main(String[] args) {
//        BIO模式
//        DApp app = new DApp(new BIOApp());
//        NIO模式
        DApp app = new DApp(new NIOApp());
        app.GET("/", ctx -> ctx.HTML(200, Utils.getResourceFile("static/index.html")));
//        注册SSE
        app.SSE("/sse", ctx -> {
//            服务器主动给浏览器推送消息
            while (true) {
                ctx.pushSSEData("hello");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
//        只支持上传文本文件,其他的会格式错误
        app.POST("/file", ctx -> {
            MultipartFile multipartFile = ctx.getMultipartFile();
            String fileName = multipartFile.getFileName();
            InputStream is = multipartFile.getInputStream();
            try {
                Files.copy(is, Paths.get(fileName));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            ctx.TEXT(200, "文件上传成功");
        });
        app.start();
    }
}
```

# 使用FastDApp搭建服务器

实现Handler和使用注解指明路由路径FastDApp会自动扫描注册

```java
import com.da.core.app.Handler;
import com.da.core.container.annotation.Get;
import com.da.core.context.Context;

@Get("/hello")
public class HelloController implements Handler {
    @Override
    public void callback(Context ctx) {
        ctx.TEXT(200, "hello world");
    }
}

```

使用FastDApp启动web服务

```java
import com.da.components.Hello;
import com.da.components.Util;
import com.da.core.app.DApp;
import com.da.core.app.FastDApp;
import com.da.core.app.impl.NIOApp;

public class App {

    public static void main(String[] args) {
//        FastDApp实例化时传入启动类是为了获取扫描的包的位置(启动类的包)
//        new NIOApp 是决定用 NIO模式
        final FastDApp app = new FastDApp(App.class, new NIOApp());
//        获取bean,使用 @Component注解标记的类
        final Hello hello = (Hello) app.getBean("Hello");
        hello.say();
//        @Component("tools") 可以指定类在容器中的名字
        final Util tools = app.getBean("tools", Util.class);
        System.out.println(tools.add(1, 1));
//        app.run(8081, false); // 自定义端口和模式
//        FastDApp.run之后会返回DApp对象
        final DApp a = app.run();
        a.GET("/aa", ctx -> ctx.TEXT(200, "aa"));
    }
}
```