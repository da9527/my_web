package com.da.core.container;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class BeanContainer
{
    private final Map<String, Object> beans = new ConcurrentHashMap<>();
    private final Map<String, Object> cache = new ConcurrentHashMap<>();

    public void addBean(String name, Object bean)
    {
        beans.put(name, bean);
    }

    public Object getBean(String name)
    {
        Object bean = cache.get(name);
        if (bean == null)
        {
            bean = beans.get(name);
            cache.put(name, bean);
        }
        return bean;
    }

    public Map<String, Object> getBeans()
    {
        return beans;
    }
}
