package com.da.core.middleware;

import com.da.core.context.Context;

public interface Middleware {
    /**
     * 执行下一个中间件
     *
     * @param ctx             上下文
     * @param middlewareChain 中间件管理者
     */
    void next(Context ctx, MiddlewareChain middlewareChain);
}
