package com.da.core.middleware;

import com.da.core.context.Context;

import java.nio.charset.StandardCharsets;

public class ErrorMiddleware implements Middleware {
    @Override
    public void next(Context ctx, MiddlewareChain middlewareChain) {
        Exception exception = ctx.getException();
        if (null != exception) {
            ctx.setStatus(500);
            ctx.setStatusMsg("OK");
            ctx.setHeader("Content-Type", "text/html;charset=utf-8;");
            String errorMsg = "<h1 style='color:red;'>服务器发生错误: [" + ctx.getException().getMessage() + "]</h1>";
            ctx.setData(errorMsg.getBytes(StandardCharsets.UTF_8));
            ctx.getException().printStackTrace();
        }
        middlewareChain.next(ctx);
    }
}
