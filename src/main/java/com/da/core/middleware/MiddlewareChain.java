package com.da.core.middleware;

import com.da.core.context.Context;

import java.util.List;

public class MiddlewareChain
{
    private final List<Middleware> middlewares;
    private int index;

    public MiddlewareChain(List<Middleware> middlewares)
    {
        this.middlewares = middlewares;
        this.index = 0;
    }

    public void next(Context ctx)
    {
        if (index >= middlewares.size())
        {
            return;
        }
        Middleware middleware = middlewares.get(index++);
        middleware.next(ctx, this);
    }

    //    每次请求需要把中间件集合的index重置
    public void init()
    {
        this.index = 0;
    }
}
