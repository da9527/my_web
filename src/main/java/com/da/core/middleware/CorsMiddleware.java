package com.da.core.middleware;

import com.da.core.context.Context;

/**
 * @Author Da
 * @Description: 三十年生死两茫茫，写程序，到天亮。
 * 千行代码，Bug何处藏。
 * 纵使上线又怎样，朝令改，夕断肠。
 * 领导每天新想法，天天改，日日忙。
 * 相顾无言，惟有泪千行。
 * 每晚灯火阑珊处，夜难寐，又加班。
 * @Date 2023/7/22 上午 7:58
 */
public class CorsMiddleware implements Middleware
{
    @Override
    public void next(Context ctx, MiddlewareChain middlewareChain)
    {
//        设置跨域
//        将 Access-Control-Allow-Origin 设置为通配符 * 可能会导致安全风险。您可以将其设置为特定的域名，以限制请求来源
        ctx.setHeader("Access-Control-Allow-Origin","*");
        ctx.setHeader("Access-Control-Allow-Headers","Authorization,X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        ctx.setHeader("Access-Control-Allow-Methods","GET, POST, OPTIONS, PATCH, PUT, DELETE");
        ctx.setHeader("Allow","GET, POST, PATCH, OPTIONS, PUT, DELETE");
        ctx.next(ctx, middlewareChain);
    }
}
