package com.da.core.middleware;


import com.da.core.context.Context;

import java.io.IOException;

public class FlushMiddleware implements Middleware {

    @Override
    public void next(Context ctx, MiddlewareChain middlewareChain) {
        ctx.next(ctx, middlewareChain);
//        刷新数据
        ctx.flashDataToResponse();
//        是否关闭通道
        if (ctx.isCloseChannel()) {
            ctx.close();
        }
    }
}
