package com.da.core.trie;

import com.da.core.app.Handler;

import java.util.HashMap;
import java.util.Map;

public class TrieNode {
    private Handler handler;
    private final Map<String, String> params;
    private final Map<String, TrieNode> children;

    public TrieNode() {
        children = new HashMap<>();
        params = new HashMap<>();
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public Handler getHandler() {
        return this.handler;
    }

    public Map<String, TrieNode> getChildren() {
        return children;
    }

    public Map<String, String> getParams() {
        return params;
    }
}
