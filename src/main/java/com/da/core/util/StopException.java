package com.da.core.util;

/**
 * @author da
 * @time 2023/10/13 上午 8:59
 * 使用异常中断流
 */
public class StopException extends RuntimeException {
    public static final StopException INSTANCE = new StopException();

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
