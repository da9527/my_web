package com.da.core.util;

import java.util.HashSet;
import java.util.function.Consumer;

/**
 * @author da
 * @time 2023/10/13 下午 3:15
 */
public class SetSeq<T> extends HashSet<T> implements Seq<T> {
    @Override
    public void consume(Consumer<T> consumer) {
        forEach(consumer);
    }
}
