package com.da.core.util;

import java.util.ArrayList;
import java.util.function.Consumer;

/**
 * @author da
 * @time 2023/10/13 上午 10:42
 */
public class ArraySeq<T> extends ArrayList<T> implements Seq<T> {
    @Override
    public void consume(Consumer<T> consumer) {
        forEach(consumer);
    }
}
