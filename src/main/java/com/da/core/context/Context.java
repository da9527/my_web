package com.da.core.context;

import com.da.core.app.BaseApp;
import com.da.core.middleware.Middleware;
import com.da.core.middleware.MiddlewareChain;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * @author da
 * @time 2023/9/17 上午 7:10
 */
public interface Context extends Middleware, OutFunc {
    /**
     * 获取请求方法
     *
     * @return 请求方法
     */
    String getMethod();

    /**
     * 获取请求 url
     *
     * @return 请求 url
     */
    String getUrl();

    /**
     * 获取请求参数
     *
     * @return 请求参数
     */
    Map<String, String> getParams();

    /**
     * 设置异常
     *
     * @param e 异常
     */
    void setException(Exception e);

    /**
     * 设置状态码
     *
     * @param code 状态码
     */
    void setStatus(int code);

    /**
     * 设置状态信息
     *
     * @param msg 状态信息
     */
    void setStatusMsg(String msg);

    /**
     * 设置请求头
     *
     * @param key   键
     * @param value 值
     */
    void setHeader(String key, String value);

    /**
     * 设置响应数据
     *
     * @param data 响应数据
     */
    void setData(byte[] data);

    /**
     * 获取异常
     *
     * @return 异常
     */
    Exception getException();

    /**
     * 设置通道是否关闭
     *
     * @param isCloseChannel 设置通道是否关闭
     */
    void setCloseChannel(boolean isCloseChannel);

    /**
     * 设置method
     *
     * @param method method
     */
    void setMethod(String method);

    /**
     * 设置 url
     *
     * @param url url
     */
    void setUrl(String url);

    /**
     * 设置 http 协议版本
     *
     * @param version http 协议版本
     */
    void setVersion(String version);

    /**
     * 获取http协议版本
     *
     * @return http协议版本
     */
    String getVersion();

    /**
     * 获取状态码
     *
     * @return 状态码
     */
    int getStatus();

    /**
     * 获取状态信息
     *
     * @return 状态信息
     */
    String getStatusMsg();

    /**
     * 获取响应头信息
     *
     * @return 响应头信息
     */
    Map<String, String> getHeaders();

    /**
     * 刷新数据到浏览器
     */
    void flashDataToResponse();

    /**
     * 是否关闭通道
     *
     * @return 是否关闭通道
     */
    boolean isCloseChannel();

    /**
     * 关闭通道
     */
    void close();

    /**
     * 构建响应头信息
     *
     * @return 响应头信息
     */
    default String buildResponseMsg() {
        StringBuffer response = new StringBuffer();
        //            拼接响应头
        response.append(this.getVersion()).append(" ");
        if (this.getStatus() == 0) {
            response.append(404).append(" ");
        } else {
            response.append(this.getStatus()).append(" ");
        }
        if (null == this.getStatusMsg()) {
            response.append("404 NOT DATA").append("\r\n");
        } else {
            response.append(this.getStatusMsg()).append("\r\n");
        }
        if (!this.getHeaders().isEmpty()) {
            this.getHeaders().forEach((k, v) -> response.append(k).append(": ").append(v).append("\r\n"));
        } else {
            response.append("Content-Type: text/html;charset=utf-8;").append("\r\n");
        }
        response.append("\r\n");
        return response.toString();
    }

    /**
     * 获取响应数据
     *
     * @return 响应数据
     */
    byte[] getData();

    /**
     * 获取上传的文件对象
     *
     * @return MultipartFile对象
     */
    MultipartFile getMultipartFile();

    @Override
    default void next(Context ctx, MiddlewareChain middlewareChain) {
        middlewareChain.next(ctx);
    }

    /**
     * 获取输入流
     *
     * @return 输入流
     */
    InputStream getInputStream();

    /**
     * 获取当前上下文的
     *
     * @return sessionId
     */
    Session getSession();

    /**
     * 获取 sessionId
     *
     * @return sessionId
     */
    String getSessionId();

    BaseApp getApp();

    void getOrCreateSession(String sessionId);
}
