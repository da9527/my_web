package com.da.core.context;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

/**
 * Session对象
 *
 * @author da
 * @time 2023/9/19 下午 3:44
 */
public class Session {
    private final Map<String, Object> data = new HashMap<>();
    private String id;

    public Session() {
    }

    public Session(String sessionId) {
        this.id = sessionId;
    }

    public String getId() {
        return id;
    }

    public void setId(String sessionId) {
        this.id = sessionId;
    }

    public Object getAttr(String name) {
        return this.data.get(name);
    }

    @SuppressWarnings("unchecked")
    public <T> T getAttr(String name, Class<T> clz) {
        return (T) this.data.get(name);
    }

    public void setAttr(String key, Object val) {
        this.data.put(key, val);
    }

    public void delAttr(String name) {
        this.data.remove(name);
    }

    public void forEach(BiConsumer<String, Object> action) {
        this.data.forEach(action);
    }
}
