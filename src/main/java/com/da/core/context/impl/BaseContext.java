package com.da.core.context.impl;

import com.da.core.app.BaseApp;
import com.da.core.context.*;
import com.da.core.util.Utils;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * @author da
 * @time 2023/9/17 上午 7:24
 */
public class BaseContext implements Context {
    //    是否关闭当前通道(默认关闭)
    private boolean closeChannel = true;
    //    请求方法
    private String method;
    //    请求URL
    private String url;
    //    响应状态码
    private int status;
    //    响应信息
    private String statusMsg;
    //    响应头
    private final Map<String, String> headers = new HashMap<>();
    //    query参数
    private final Map<String, String> params = new HashMap<>();
    //    http协议版本
    private String version;
    //    响应文本
    private byte[] data;
    private Exception error;
    //    请求对象
    protected Request request;
    //    响应对象
    protected Response response;

    @Override
    public String getMethod() {
        return this.method;
    }

    @Override
    public String getUrl() {
        return this.url;
    }

    @Override
    public Map<String, String> getParams() {
        return this.params;
    }

    @Override
    public void setException(Exception e) {
        this.error = e;
    }

    @Override
    public void setStatus(int code) {
        this.status = code;
    }

    @Override
    public void setStatusMsg(String msg) {
        this.statusMsg = msg;
    }

    @Override
    public void setHeader(String key, String value) {
        this.headers.put(key, value);
    }

    @Override
    public void setData(byte[] data) {
        this.data = data;
    }

    @Override
    public Exception getException() {
        return error;
    }

    @Override
    public void setCloseChannel(boolean isCloseChannel) {
        this.closeChannel = isCloseChannel;
    }

    @Override
    public void setMethod(String method) {
        this.method = method;
    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String getVersion() {
        return this.version;
    }

    @Override
    public int getStatus() {
        return this.status;
    }

    @Override
    public String getStatusMsg() {
        return this.statusMsg;
    }

    @Override
    public Map<String, String> getHeaders() {
        return this.headers;
    }

    @Override
    public void flashDataToResponse() {
    }

    @Override
    public boolean isCloseChannel() {
        return this.closeChannel;
    }

    @Override
    public void close() {
    }

    @Override
    public byte[] getData() {
        return this.data;
    }

    @Override
    public void TEXT(int code, String message) {
        this.response.TEXT(code, message);
    }

    @Override
    public void HTML(int code, String message) {
        this.response.HTML(code, message);
    }

    @Override
    public void HTML(int code, File html) {
        this.response.HTML(code, html);
    }

    @Override
    public void JSON(int code, String message) {
        this.response.JSON(code, message);
    }

    @Override
    public void FILE(int code, File file) {
        this.response.FILE(code, file);
    }

    @Override
    public void pushSSEData(String message) {
        this.response.pushSSEData(message);
    }

    @Override
    public MultipartFile getMultipartFile() {
        return this.request.getMultipartFile();
    }

    @Override
    public InputStream getInputStream() {
        return null;
    }

    @Override
    public void getOrCreateSession(String sessionId) {
        Session session = this.getApp().getSession();
//        如果不存在就创建
        if (null == session) {
//            生成id
            sessionId = Utils.generateSessionId();
            this.getApp().setSession(new Session(sessionId));
//          响应 sessionId
            this.setHeader("Set-Cookie", "session=" + sessionId);
        }
    }

    @Override
    public Session getSession() {
        return this.getApp().getSession();
    }

    @Override
    public String getSessionId() {
        if (this.getParams().containsKey("Cookie")) {
            String cookieInfo = this.getParams().get("Cookie");
            String[] cookies = cookieInfo.split(" ");
            for (String cookie : cookies) {
                if (cookie.startsWith("session=")) {
                    return cookie.substring("session=".length());
                }
            }
        }
        return null;
    }

    @Override
    public BaseApp getApp() {
        return null;
    }
}
