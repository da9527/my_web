package com.da.core.context;

import java.io.File;

/**
 * @author da
 * @time 2023/9/16 下午 1:42
 */
public interface OutFunc {
    /**
     * 响应文本到浏览器
     *
     * @param code    状态码
     * @param message 文本信息
     */
    void TEXT(int code, String message);

    /**
     * 响应html到浏览器
     *
     * @param code    状态码
     * @param message html字符串
     */
    void HTML(int code, String message);

    /**
     * 响应html到浏览器
     *
     * @param code 状态码
     * @param html html文件
     */
    void HTML(int code, File html);

    /**
     * 响应json到浏览器
     *
     * @param code    状态码
     * @param message json类型的字符串
     */
    void JSON(int code, String message);

    /**
     * 响应文件到浏览器
     *
     * @param code 状态码
     * @param file 文件
     */
    void FILE(int code, File file);

    /**
     * 拼接SSE传输的数据格式
     *
     * @param message 文字内容
     */
    void pushSSEData(String message);
}
