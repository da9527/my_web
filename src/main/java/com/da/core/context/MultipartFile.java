package com.da.core.context;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * @author da
 * @time 2023/9/17 上午 10:24
 */
public class MultipartFile {
    private String fieldName;
    private String fileName;
    private Map<String, String> headers = new HashMap<>();
    private InputStream inputStream;

    public MultipartFile() {
    }

    public MultipartFile(String fieldName, String fileName, Map<String, String> headers, InputStream inputStream) {
        this.fieldName = fieldName;
        this.fileName = fileName;
        this.headers = headers;
        this.inputStream = inputStream;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFileName() {
        return fileName;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setHeader(String k, String v) {
        this.headers.put(k, v);
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public boolean isFile() {
        return fileName != null && !fileName.isEmpty();
    }

    public String getValue() throws IOException {
        byte[] bytes = new byte[inputStream.available()];
        int read = inputStream.read(bytes);
        if (read < 0) throw new IOException("读取内容失败");
        return new String(bytes, StandardCharsets.UTF_8);
    }
}
