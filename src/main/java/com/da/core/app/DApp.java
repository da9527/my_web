package com.da.core.app;

public class DApp {
    //    端口
    private int port = 8080;
    //    服务器
    private final BaseApp app;

    public DApp(BaseApp app) {
        this.app = app;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void start() {
        new Thread(() -> app.start(port)).start();
    }

    public void start(int port) {
        new Thread(() -> app.start(port)).start();
    }


    public void GET(String path, Handler func) {
        app.GET(path, func);
    }

    public void POST(String path, Handler func) {
        app.POST(path, func);
    }

    public void PUT(String path, Handler func) {
        app.PUT(path, func);
    }

    public void DELETE(String path, Handler func) {
        app.DELETE(path, func);
    }

    public void SSE(String path, Handler func) {
        app.SSE(path, func);
    }
}
