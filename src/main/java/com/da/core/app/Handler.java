package com.da.core.app;

import com.da.core.context.Context;

public interface Handler {
    /**
     * 对应path的处理器
     *
     * @param ctx 上下文
     */
    void callback(Context ctx);
}
